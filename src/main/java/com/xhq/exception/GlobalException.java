package com.xhq.exception;

import com.xhq.entity.R;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
@ResponseBody
public class GlobalException {
	@ExceptionHandler(value = Exception.class)
	public R exceptionhandler(HttpServletRequest request, Exception e){
              return R.error("服务端异常,请联系管理员"+"<br/>"+e.getMessage());
	}
}
