package com.xhq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
* 政策实体类
* */
@TableName("zhengce")
@Data
public class ZhengCe {
    @TableId(type = IdType.AUTO)
    private Integer zcid;//政策id

    private String zcbiaoti;//标题

    private String neirong;//内容

    private Integer dianji;//点击量

    public Integer getZcid() {
        return zcid;
    }

    public void setZcid(Integer zcid) {
        this.zcid = zcid;
    }

    public String getZcbiaoti() {
        return zcbiaoti;
    }

    public void setZcbiaoti(String zcbiaoti) {
        this.zcbiaoti = zcbiaoti;
    }

    public String getNeirong() {
        return neirong;
    }

    public void setNeirong(String neirong) {
        this.neirong = neirong;
    }

    public Integer getDianji() {
        return dianji;
    }

    public void setDianji(Integer dianji) {
        this.dianji = dianji;
    }
}
