package com.xhq.entity;


import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 商品大类

 */
@TableName("liuyan")
@Data
public class LiuYan {

    private Integer lid;//留言编号

    private String xiang;//留言详情

    private Integer lman;//留言人

    @TableField(select = false,insertStrategy = FieldStrategy.NEVER,updateStrategy = FieldStrategy.NEVER)
    private Pinkun pinKun;  // 所属留言人

    public Pinkun getPinKun(){return pinKun;}

    public void setPinKun(Pinkun pinKun){this.pinKun=pinKun;}

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        this.lid = lid;
    }

    public String getXiang() {
        return xiang;
    }

    public void setXiang(String xiang) {
        this.xiang = xiang;
    }

    public Integer getLman() {
        return lman;
    }

    public void setLman(Integer lman) {
        this.lman = lman;
    }
}
