package com.xhq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import javax.persistence.*;
import java.security.SecureRandom;
import java.util.List;

/*
* 贫困户
* */

@Entity
@TableName("pinkun")
public class Pinkun {
    @TableId(type = IdType.AUTO)
    private Integer pid;
    private String pname;
    private String psex;
    private String pxiangqing;
    private String pnumber;
    private String pimg;
    private String addtime;
    private String zhuzhi;

    // 省略 getter 和 setter

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPsex() {
        return psex;
    }

    public void setPsex(String psex) {
        this.psex = psex;
    }

    public String getPxiangqing() {
        return pxiangqing;
    }

    public void setPxiangqing(String pxiangqing) {
        this.pxiangqing = pxiangqing;
    }

    public String getPnumber() {
        return pnumber;
    }

    public void setPnumber(String pnumber) {
        this.pnumber = pnumber;
    }

    public String getPimg() {
        return pimg;
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getZhuzhi() {
        return zhuzhi;
    }

    public void setZhuzhi(String zhuzhi) {
        this.zhuzhi = zhuzhi;
    }
}

