package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.ZhengCe;


/**
 * 政策Service接口

 */
public interface IZhengCeService extends IService<ZhengCe> {

    /**
     * 修改
     * @param zhengCe
     * @return
     */
    public Integer update(ZhengCe zhengCe);

}
