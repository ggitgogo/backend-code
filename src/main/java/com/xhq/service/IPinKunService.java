package com.xhq.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.Pinkun;

/*
* 贫困户service接口
* */
public interface IPinKunService extends IService<Pinkun>{
}
