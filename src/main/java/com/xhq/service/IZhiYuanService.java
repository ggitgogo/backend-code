package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.ZhiYuan;

/*
* 志愿者Servie接口
* */
public interface IZhiYuanService extends IService<ZhiYuan> {
}
