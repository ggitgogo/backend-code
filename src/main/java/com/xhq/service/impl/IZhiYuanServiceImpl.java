package com.xhq.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.ZhiYuan;
import com.xhq.mapper.ZhiYuanMpper;
import com.xhq.service.IZhiYuanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
* 志愿者实现类
* */



@Service("ZhiYuanService")
public class IZhiYuanServiceImpl extends ServiceImpl<ZhiYuanMpper, ZhiYuan> implements IZhiYuanService {
    @Autowired
    private ZhiYuanMpper zhiYuanMpper;
}
