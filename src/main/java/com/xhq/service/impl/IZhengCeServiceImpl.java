package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.ZhengCe;
import com.xhq.mapper.ZhengCeMapper;
import com.xhq.service.IZhengCeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 政策Service实现类

 */
@Service("zhengCeService")
public class IZhengCeServiceImpl extends ServiceImpl<ZhengCeMapper, ZhengCe> implements IZhengCeService {

    @Autowired
    private ZhengCeMapper zhengCeMapper;


    @Override
    public Integer update(ZhengCe zhengCe) {
        return zhengCeMapper.update(zhengCe);
    }
}
