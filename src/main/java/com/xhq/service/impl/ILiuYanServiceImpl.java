package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.LiuYan;
import com.xhq.mapper.LiuYanMapper;
import com.xhq.service.ILiuYanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//商品大类
@Service("liuyanService")
public class ILiuYanServiceImpl extends ServiceImpl<LiuYanMapper, LiuYan> implements ILiuYanService {
	@Autowired
	private LiuYanMapper liuYanMapper;
}
