package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.BigType;
import com.xhq.mapper.BigTypeMapper;
import com.xhq.service.IBigTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//商品大类
@Service("bigtypeService")
public class IBigTypeServiceImpl extends ServiceImpl<BigTypeMapper, BigType> implements IBigTypeService {
	@Autowired
	private BigTypeMapper bigTypeMapper;
}
