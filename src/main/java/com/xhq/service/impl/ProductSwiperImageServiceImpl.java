package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.ProductSwiperImage;
import com.xhq.mapper.ProductSwiperImageMapper;
import com.xhq.service.IProductSwiperImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//商品大类
@Service("ProductSwiperImageService")
public class ProductSwiperImageServiceImpl extends ServiceImpl<ProductSwiperImageMapper, ProductSwiperImage> implements IProductSwiperImageService {
	@Autowired
	private ProductSwiperImageMapper productSwiperImageMapper;
}
