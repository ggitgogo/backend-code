package com.xhq.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.Juanzeng;
import com.xhq.entity.Pinkun;
import com.xhq.mapper.JuanzengMapper;
import com.xhq.mapper.PinkunMapper;
import com.xhq.service.JuanzengService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/*
* 捐赠实现类
* */


@Service
public class JuanzengServiceImpl extends ServiceImpl<JuanzengMapper, Juanzeng> implements JuanzengService{
}

