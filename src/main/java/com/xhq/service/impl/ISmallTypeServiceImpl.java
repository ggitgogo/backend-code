package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.SmallType;
import com.xhq.mapper.SmallTypeMapper;
import com.xhq.service.ISmallTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("smalltypeService")
public class ISmallTypeServiceImpl extends ServiceImpl<SmallTypeMapper, SmallType> implements ISmallTypeService {
	@Autowired
	private SmallTypeMapper smallTypeMapper;
	@Override
	public List<SmallType> list(Map<String, Object> map) {
		return smallTypeMapper.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return smallTypeMapper.getTotal(map);
	}

	@Override
	public Integer add(SmallType smallType) {
		return smallTypeMapper.add(smallType);
	}

	@Override
	public Integer update(SmallType smallType) {
		return smallTypeMapper.update(smallType);
	}

}
