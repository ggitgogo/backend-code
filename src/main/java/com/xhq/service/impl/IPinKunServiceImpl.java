package com.xhq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhq.entity.Pinkun;
import com.xhq.mapper.PinkunMapper;
import com.xhq.service.IPinKunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
* 贫困户实现类
* */
@Service("PinKunService")
public class IPinKunServiceImpl extends ServiceImpl<PinkunMapper,Pinkun> implements IPinKunService {

    @Autowired
    private PinkunMapper pinKunMapper;
}
