package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.BigType;


public interface IBigTypeService extends IService<BigType> {
}
