package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.OrderDetail;


/**
 * 订单细表Service接口

 */
public interface IOrderDetailService extends IService<OrderDetail> {


}
