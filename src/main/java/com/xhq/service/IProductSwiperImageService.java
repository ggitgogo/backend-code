package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.BigType;
import com.xhq.entity.ProductSwiperImage;


public interface IProductSwiperImageService extends IService<ProductSwiperImage> {
}
