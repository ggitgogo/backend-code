package com.xhq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhq.entity.WxUserInfo;


/**
 * 微信用户Service接口

 */
public interface IWxUserInfoService extends IService<WxUserInfo> {


}
