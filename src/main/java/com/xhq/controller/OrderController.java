package com.xhq.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.Order;
import com.xhq.entity.OrderDetail;
import com.xhq.entity.R;
import com.xhq.mapper.OrderDetailMapper;
import com.xhq.service.IOrderDetailService;
import com.xhq.service.IOrderService;
import com.xhq.utils.DateUtil;
import com.xhq.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/my/order")
public class OrderController {
	@Autowired
	IOrderService orderService;
	@Autowired
	IOrderDetailService orderDetailService;
	@PostMapping("/pay")
	public R payment(String orderNo){
		System.out.println("********orderno="+orderNo);
		Order order=orderService.getOne(new QueryWrapper<Order>().eq("orderNo",orderNo));
		if(order!=null&&order.getStatus()==1){
			order.setPayDate(new Date());
			order.setStatus(2);
			//orderService.save(order);
			orderService.updateById(order);
			Map<String,Object> resultmap=new HashMap<>();

			resultmap.put("paymentres","ok");
			System.out.println("支付成功");
			return R.ok(resultmap);//成功就返回1

		}
         else {
			System.out.println("支付失败");

			Map<String,Object> resultmap=new HashMap<>();
			resultmap.put("paymentres","false");
			return R.ok(resultmap); //失败就返回0

		}
	}
	@RequestMapping("/create")
	@Transactional
	public R create(@RequestBody Order order, @RequestHeader(value = "token") String token){
      //通过token获得openid
		System.out.println("order="+order);
		System.out.println("token="+token);
		Claims claims= JwtUtils.validateJWT(token).getClaims();
		if(claims!=null){
          order.setUserId(claims.getId());
		}
		order.setOrderNo("DD"+ DateUtil.getCurrentDateStr());
		order.setCreateDate(new Date());
		OrderDetail[] goods=order.getGoods();
		orderService.save(order);
		for(int i=0;i<goods.length;i++){
			OrderDetail orderDetail=goods[i];
			orderDetail.setMId(order.getId());
			orderDetailService.save(orderDetail);
		}
		Map<String,Object> resultmap=new HashMap<>();
		resultmap.put("orderNo",order.getOrderNo());
		return R.ok(resultmap);




	}
	//查询订单信息  根据type
	@RequestMapping("/list")
	public R list(Integer type,Integer page,Integer pagesize){
		System.out.println(type);
		System.out.println(page);
		System.out.println(pagesize);
		List<Order> orderlist=null;
		Map<String,Object> resultmap=new HashMap<>();
		Page<Order> pageorder=new Page<>(page,pagesize);
		if(type==0){
			//orderlist=orderService.list(new QueryWrapper<Order>().orderByDesc("id"));
		 Page<Order> orderresult=orderService.page(pageorder,new QueryWrapper<Order>().orderByDesc("id"));
		 orderlist=orderresult.getRecords();
		 resultmap.put("total",orderresult.getTotal());
		 resultmap.put("totalpage",orderresult.getPages());

		}else{
			//orderlist=orderService.list(new QueryWrapper<Order>().eq("status",type).orderByDesc("id"));
			Page<Order> orderresult=orderService.page(pageorder,new QueryWrapper<Order>().eq("status",type).orderByDesc("id"));
			orderlist=orderresult.getRecords();
			resultmap.put("total",orderresult.getTotal());
			resultmap.put("totalpage",orderresult.getPages());
		}
		resultmap.put("orderlist",orderlist);
		System.out.println(orderlist);
		return R.ok(resultmap);
	}
}
