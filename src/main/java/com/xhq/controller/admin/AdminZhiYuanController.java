package com.xhq.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.PageBean;
import com.xhq.entity.ZhiYuan;
import com.xhq.entity.R;
import com.xhq.service.IZhiYuanService;
import com.xhq.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/*
志愿者Controller
* */

@Slf4j
@RestController
@RequestMapping("/admin/zhiY")
public class AdminZhiYuanController {

    @Autowired
    private IZhiYuanService zhiYuanService;

    @Value("${zhiYuanImagesFilePath}")
    private String zhiYuanImagesFilePath;

    //查询所有贫困户
    /**
     * 分页查询
     * @param pageBean
     * @return
     */
    @RequestMapping("/list")
    public R list(@RequestBody PageBean pageBean){
        System.out.println(pageBean);
        String query=pageBean.getQuery().trim();
        Page<ZhiYuan> page=new Page<>(pageBean.getPageNum(),pageBean.getPageSize());
        Page<ZhiYuan> pageResult = zhiYuanService.page(page, new QueryWrapper<ZhiYuan>().like("vname", query));
        Map<String,Object> map=new HashMap<>();
        map.put("zhiYuanList",pageResult.getRecords());
        map.put("total",pageResult.getTotal());
        return R.ok(map);
    }

    /**
     * 查询所有数据 下拉框用到
     * @return
     */
    @RequestMapping("/listAll")
    public R listAll(){
        Map<String,Object> map=new HashMap<>();
        map.put("zhiYuanList",zhiYuanService.list());
        return R.ok(map);
    }

    /**
     * 添加或者修改
     * @param zhiYuan
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody ZhiYuan zhiYuan){
        log.info("zhiYuan:{}",zhiYuan);

        if(zhiYuan.getVid()==null || zhiYuan.getVid()==-1){
            zhiYuanService.save(zhiYuan);
        }else{
            System.out.println("___________________"+zhiYuan.getVname());
            zhiYuanService.updateById(zhiYuan);
            System.out.println("_________________________"+zhiYuan);
        }
        return R.ok();
    }

    /**
     * 删除
     * @param vid
     * @return
     */
    @GetMapping("/delete/{vid}")
    public R delete(@PathVariable(value = "vid") Integer vid){
            zhiYuanService.removeById(vid);
            return R.ok();
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R findById(@PathVariable(value = "id") Integer id){
        System.out.println("id="+id);
        ZhiYuan zhiYuan = zhiYuanService.getById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("zhiYuan",zhiYuan);
        return R.ok(map);
    }

    /**
     * 上传志愿者图片
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping("/uploadImage")
    public Map<String,Object> uploadImage(MultipartFile file)throws Exception{
        Map<String,Object> map=new HashMap<String,Object>();
        if(!file.isEmpty()){
            // 获取文件名
            String fileName = file.getOriginalFilename();
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            String newFileName= DateUtil.getCurrentDateStr()+suffixName;
            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(zhiYuanImagesFilePath+newFileName));
            map.put("code", 0);
            map.put("msg", "上传成功");
            Map<String,Object> map2=new HashMap<String,Object>();
            map2.put("title", newFileName);
            map2.put("src", "/image/zhiYuan/"+newFileName);
            map.put("data", map2);
        }

        return map;
    }

}
