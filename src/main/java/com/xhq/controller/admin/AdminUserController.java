package com.xhq.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.PageBean;
import com.xhq.entity.R;
import com.xhq.entity.WxUserInfo;
import com.xhq.service.IWxUserInfoService;
import com.xhq.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//用户管理
@RestController
@RequestMapping("/admin/user")
public class AdminUserController {
	@Autowired
	private IWxUserInfoService wxUserInfoService;
	@RequestMapping("/list")
  public R list(@RequestBody PageBean pageBean){
		System.out.println(pageBean);
        String query=pageBean.getQuery().trim();
		System.out.println(query);
		Page<WxUserInfo> page=new Page<>(pageBean.getPageNum(),pageBean.getPageSize());
		Page<WxUserInfo> pageResult=wxUserInfoService.page(page,new QueryWrapper<WxUserInfo>().like("nickName",query));
		Map<String,Object> map=new HashMap<>();
		map.put("userList",pageResult.getRecords());
		map.put("total",pageResult.getTotal());
		return R.ok(map);
  }

}
