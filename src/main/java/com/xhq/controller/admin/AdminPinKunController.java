package com.xhq.controller.admin;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.PageBean;
import com.xhq.entity.Pinkun;
import com.xhq.entity.R;
import com.xhq.service.IPinKunService;

import com.xhq.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/*
贫困户Controller
* */

@Slf4j
@RestController
@RequestMapping("/admin/pinKun")
public class AdminPinKunController {

    @Autowired
    private IPinKunService pinKunService;

    @Value("${pinKunImagesFilePath}")
    private String pinKunImagesFilePath;

    //查询所有贫困户
    /**
     * 分页查询
     * @param pageBean
     * @return
     */
    @RequestMapping("/list")
    public R list(@RequestBody PageBean pageBean){
        System.out.println(pageBean);
        String query=pageBean.getQuery().trim();
        Page<Pinkun> page=new Page<>(pageBean.getPageNum(),pageBean.getPageSize());
        Page<Pinkun> pageResult = pinKunService.page(page, new QueryWrapper<Pinkun>().like("pname", query));
        Map<String,Object> map=new HashMap<>();
        map.put("pinKunList",pageResult.getRecords());
        map.put("total",pageResult.getTotal());
        return R.ok(map);
    }

    /**
     * 查询所有数据 下拉框用到
     * @return
     */
    @RequestMapping("/listAll")
    public R listAll(){
        Map<String,Object> map=new HashMap<>();
        map.put("pinKunList",pinKunService.list());
        return R.ok(map);
    }

    /**
     * 添加或者修改
     * @param pinKun
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody Pinkun pinKun){
        log.info("pinKun:{}",pinKun);

        if(pinKun.getPid()==null || pinKun.getPid()==-1){
            pinKunService.save(pinKun);
        }else{
            System.out.println("___________________"+pinKun.getPname());
            pinKunService.updateById(pinKun);
            System.out.println("_________________________"+pinKun);
        }
        return R.ok();
    }

    /**
     * 删除
     * @param pid
     * @return
     */
    @GetMapping("/delete/{pid}")
    public R delete(@PathVariable(value = "pid") Integer pid){
            pinKunService.removeById(pid);
            return R.ok();
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R findById(@PathVariable(value = "id") Integer id){
        System.out.println("id="+id);
        Pinkun pinKun = pinKunService.getById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("pinKun",pinKun);
        return R.ok(map);
    }

    /**
     * 上传贫困户图片
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping("/uploadImage")
    public Map<String,Object> uploadImage(MultipartFile file)throws Exception{
        Map<String,Object> map=new HashMap<String,Object>();
        if(!file.isEmpty()){
            // 获取文件名
            String fileName = file.getOriginalFilename();
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            String newFileName= DateUtil.getCurrentDateStr()+suffixName;
            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(pinKunImagesFilePath+newFileName));
            map.put("code", 0);
            map.put("msg", "上传成功");
            Map<String,Object> map2=new HashMap<String,Object>();
            map2.put("title", newFileName);
            map2.put("src", "/image/pinKun/"+newFileName);
            map.put("data", map2);
        }

        return map;
    }

}
