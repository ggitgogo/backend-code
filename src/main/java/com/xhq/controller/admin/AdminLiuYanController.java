package com.xhq.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.LiuYan;
import com.xhq.entity.PageBean;
import com.xhq.entity.R;
import com.xhq.entity.SmallType;
import com.xhq.service.ILiuYanService;
import com.xhq.service.ISmallTypeService;
import com.xhq.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 管理端-留言Controller控制器

 */
@Slf4j
@RestController
@RequestMapping("/admin/liuYan")
public class AdminLiuYanController {

    @Autowired
    private ILiuYanService liuYanService;
    

    /**
     * 分页查询
     * @param pageBean
     * @return
     */
    @RequestMapping("/list")
    public R list(@RequestBody PageBean pageBean){
        System.out.println(pageBean);
        String query=pageBean.getQuery().trim();
        Page<LiuYan> page=new Page<>(pageBean.getPageNum(),pageBean.getPageSize());
        Page<LiuYan> pageResult = liuYanService.page(page, new QueryWrapper<LiuYan>().like("name", query));
        Map<String,Object> map=new HashMap<>();
        map.put("liuYanList",pageResult.getRecords());
        map.put("total",pageResult.getTotal());
        return R.ok(map);
    }

    /**
     * 查询所有数据 下拉框用到
     * @return
     */
    @RequestMapping("/listAll")
    public R listAll(){
        Map<String,Object> map=new HashMap<>();
        map.put("liuYanList",liuYanService.list());
        return R.ok(map);
    }

    /**
     * 添加或者修改
     * @param liuYan
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody LiuYan liuYan){
        log.info("liuYan:{}",liuYan);

        if(liuYan.getLid()==null || liuYan.getLid()==-1){
            liuYanService.save(liuYan);
        }else{
            System.out.println("___________________"+liuYan.getLman());
            liuYanService.updateById(liuYan);
            System.out.println("_________________________"+liuYan);
        }
        return R.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @GetMapping("/delete/{id}")
    public R delete(@PathVariable(value = "id") Integer id){
        liuYanService.removeById(id);
        return R.ok();
    }


    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R findById(@PathVariable(value = "id") Integer id){
        System.out.println("id="+id);
        LiuYan liuYan = liuYanService.getById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("liuYan",liuYan);
        return R.ok(map);
    }




}
