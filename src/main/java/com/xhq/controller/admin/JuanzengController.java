package com.xhq.controller.admin;

import com.xhq.entity.Juanzeng;
import com.xhq.entity.R;
import com.xhq.mapper.JuanzengMapper;
import com.xhq.mapper.PinkunMapper;
import com.xhq.service.JuanzengService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/juanzeng")
public class JuanzengController {
    @Autowired
    private JuanzengService juanzengService;
    @Autowired
    private PinkunMapper pinkunMapper;
    @Autowired
    private JuanzengMapper juanzengMapper;

    @GetMapping("/all")
    public List<Juanzeng> findAll() {
        return juanzengMapper.getJuanzengWithPinkun(null);
    }

    @GetMapping("/{jid}")
    public List<Juanzeng> findJuanzengByPid(@PathVariable Integer jid) {
        return juanzengMapper.getJuanzengWithPinkun(jid);
    }

    /**
     * 删除
     * @param jid
     * @return
     */
    @GetMapping("/delete/{jid}")
    public R delete(@PathVariable(value = "jid") Integer jid){
        juanzengService.removeById(jid);
        return R.ok();
    }

}
