package com.xhq.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.ZhengCe;
import com.xhq.entity.PageBean;
import com.xhq.entity.R;
import com.xhq.service.IZhengCeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.HashMap;
import java.util.List;



/**
 * 管理端-政策Controller控制器

 */
@RestController
@RequestMapping("/admin/zhengce")
public class AdminZhengCeController {

    @Autowired
    private IZhengCeService zhengCeService;

    //查询所有政策信息
    /**
     * 分页查询
     * @param pageBean
     * @return
     */
    @RequestMapping("/list")
    public R list(@RequestBody PageBean pageBean){
        System.out.println(pageBean);
        String query=pageBean.getQuery().trim();
        Page<ZhengCe> page=new Page<>(pageBean.getPageNum(),pageBean.getPageSize());
        Page<ZhengCe> pageResult = zhengCeService.page(page, new QueryWrapper<ZhengCe>().like("zcbiaoti", query));
        Map<String,Object> map=new HashMap<>();
        map.put("zhengCeList",pageResult.getRecords());
        map.put("total",pageResult.getTotal());
        return R.ok(map);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R findById(@PathVariable(value = "id") Integer id){
        System.out.println("zcid="+id);
        ZhengCe zhengCe = zhengCeService.getById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("zhengCe",zhengCe);
        return R.ok(map);
    }

    /**
     * 添加或者修改
     * @param zhengCe
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody ZhengCe zhengCe){
//        log.info("zhengCe:{}",zhengCe);

        if(zhengCe.getZcid()==null || zhengCe.getZcid()==-1){
            zhengCeService.save(zhengCe);
        }else{
            System.out.println("___________________"+zhengCe.getZcbiaoti());
            zhengCeService.updateById(zhengCe);
            System.out.println("_________________________"+zhengCe);
        }
        return R.ok();
    }

    /**
     * 删除
     * @param zcid
     * @return
     */
    @GetMapping("/delete/{zcid}")
    public R delete(@PathVariable(value = "zcid") Integer zcid){
        zhengCeService.removeById(zcid);
        return R.ok();
    }

    /*
    * 发布政策
    * @param id
    * @return
    * */


}
