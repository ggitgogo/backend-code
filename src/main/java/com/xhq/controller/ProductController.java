package com.xhq.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhq.entity.Product;
import com.xhq.entity.ProductSwiperImage;
import com.xhq.entity.R;
import com.xhq.service.IProductService;
import com.xhq.service.IProductSwiperImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private IProductService productService;
	@Autowired
	IProductSwiperImageService productSwiperImageService;
//	查询轮播商品
	@GetMapping("/findswiper")
	public R findSwiper(){
      List<Product> swiperProductlist =productService.list(new QueryWrapper<Product>().eq("isSwiper",true).orderByAsc("swiperSort"));
		Map<String,Object> map=new HashMap<>();
		map.put("message",swiperProductlist);
      return R.ok(map);
	}
	@GetMapping("/findhot")
	public R findhot(){
		Page<Product> page=new Page<>(0,8);
		Page<Product> pageProduct=productService.page(page,new QueryWrapper<Product>().eq("isHot",true).orderByAsc("hotDateTime"));
	   List<Product> hotproductlist=pageProduct.getRecords();
		Map<String,Object> map=new HashMap<>();
		map.put("message",hotproductlist);
		return R.ok(map);
	}
	//通过商品id获得商品详情页面的轮播图片
	@GetMapping("/detail")
	public R detail(Integer id){
		 Product product=productService.getById(id);
		List<ProductSwiperImage> productSwiperImagelist= productSwiperImageService.list(new QueryWrapper<ProductSwiperImage>().eq("productId",product.getId()).orderByAsc("sort"));
         product.setProductSwiperImageList(productSwiperImagelist);
		Map<String,Object> map=new HashMap<>();
		map.put("message",product);
		return R.ok(map);
	}
    @GetMapping("/search")
	public R search(String q){
     List<Product> productList=productService.list(new QueryWrapper<Product>().like("name",q));
		Map<String,Object> map=new HashMap<>();
		map.put("message",productList);
		return R.ok(map);
	}
}
