package com.xhq.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhq.constant.SystemConstant;
import com.xhq.entity.R;
import com.xhq.entity.WxUserInfo;
import com.xhq.properties.WeixinProperties;
import com.xhq.service.IWxUserInfoService;
import com.xhq.utils.HttpClientUtil;
import com.xhq.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private WeixinProperties weixinProperties;
	@Autowired
	private HttpClientUtil httpClientUtil;
	@Autowired
	private IWxUserInfoService wxUserInfoService;
	//微信用户登入
	@RequestMapping("/wxlogin")
	public R wxLogin(@RequestBody WxUserInfo wxUserInfo){
		 wxUserInfo.getCode();
		String jscode2sessionUrl=weixinProperties.getJscode2sessionUrl()+"?appid="+weixinProperties.getAppid()+"&secret="+weixinProperties.getSecret()+"&js_code="+wxUserInfo.getCode()+"&grant_type=authorization_code";
		String result=httpClientUtil.sendHttpGet(jscode2sessionUrl);
		System.out.println(jscode2sessionUrl);
		JSONObject jsonObject= JSON.parseObject(result);
		 String openid=jsonObject.get("openid").toString();
		 //插入用户到数据库

		 WxUserInfo wxUserInforesult=wxUserInfoService.getOne(new QueryWrapper<WxUserInfo>().eq("openid",openid));
		 if(wxUserInforesult==null){
		 	wxUserInfo.setOpenid(openid);
		 	wxUserInfo.setRegisterDate(new Date());
		 	wxUserInfo.setLastLoginDate(new Date());
		 	wxUserInfoService.save(wxUserInfo);
		 }else{
		 	wxUserInforesult.setNickName(wxUserInfo.getNickName());
		 	wxUserInforesult.setAvatarUrl(wxUserInfo.getAvatarUrl());
		 	wxUserInforesult.setLastLoginDate(new Date());
		 	wxUserInfoService.updateById(wxUserInforesult);
		 }
		//	利用jwt生产token返回前端
		String token=JwtUtils.createJWT(openid,wxUserInfo.getNickName(),SystemConstant.JWT_TTL);
		Map<String,Object> resultmap=new HashMap<>();
		resultmap.put("token",token);
		System.out.println("*****************"+token);
		return R.ok(resultmap);
	}
}
