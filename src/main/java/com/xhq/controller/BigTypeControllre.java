package com.xhq.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhq.entity.BigType;
import com.xhq.entity.Product;
import com.xhq.entity.R;
import com.xhq.entity.SmallType;
import com.xhq.service.IBigTypeService;
import com.xhq.service.IProductService;
import com.xhq.service.ISmallTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bigtype")
public class BigTypeControllre {
	@Autowired
	private IBigTypeService bigTypeService;
	@Autowired
	private ISmallTypeService smalllTypeService;
	@Autowired
	private IProductService productService;
	//查询所有商品大类
	@GetMapping("/findall")
	public R findall(){
       List<BigType> bigtypelist = bigTypeService.list();
		Map<String,Object> map=new HashMap<>();
		map.put("message",bigtypelist);
		return R.ok(map);
	}
	//获取所有菜单信息
	@GetMapping("/findcategories")
	public R findcategories(){
		List<BigType> bigtypelist = bigTypeService.list();
		//将大类里面的小类查询出来再放进大类去
		for (BigType bigType: bigtypelist) {
			List<SmallType> smallTypeList= smalllTypeService.list(new QueryWrapper<SmallType>().eq("bigTypeId",bigType.getId()));
			bigType.setSmallTypeList(smallTypeList);
			for (SmallType smallType:smallTypeList) {
                List<Product> productList=productService.list(new QueryWrapper<Product>().eq("typeId",smallType.getId()));
                smallType.setProductList(productList);
			}
		}
		Map<String,Object> map=new HashMap<>();
		map.put("message",bigtypelist);
		return R.ok(map);
	}

}
