package com.xhq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xhq.mapper")
public class MallshoppingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallshoppingApplication.class, args);
	}

}
