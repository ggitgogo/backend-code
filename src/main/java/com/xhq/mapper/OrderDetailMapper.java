package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.OrderDetail;


/**
 * 订单细表Mapper接口

 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {


}
