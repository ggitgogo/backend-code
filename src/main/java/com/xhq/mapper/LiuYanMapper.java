package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.LiuYan;


public interface LiuYanMapper extends BaseMapper<LiuYan> {
	/**
	 * 根据id查询留言信息
	 * @param id
	 * @return
	 */
	public LiuYan findById(Integer id);
}
