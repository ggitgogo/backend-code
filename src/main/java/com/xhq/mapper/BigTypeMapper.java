package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.BigType;


public interface BigTypeMapper extends BaseMapper<BigType> {
	/**
	 * 根据id查询商品大类
	 * @param id
	 * @return
	 */
	public BigType findById(Integer id);
}
