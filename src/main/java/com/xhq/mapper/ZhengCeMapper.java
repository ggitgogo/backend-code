package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.ZhengCe;

/*
* 政策Mapper接口
* */
public interface ZhengCeMapper extends BaseMapper<ZhengCe>{
    /**
     * 更新
     * @param zhengCe
     * @return
     */
    public Integer update(ZhengCe zhengCe);
}
