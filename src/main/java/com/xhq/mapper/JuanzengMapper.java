package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.Juanzeng;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/*
 * 捐赠Mapper接口
 * */

@Mapper
public interface JuanzengMapper extends BaseMapper<Juanzeng>{
    @Select("SELECT * FROM juanzeng")
    List<Juanzeng> findAll();
    List<Juanzeng> getJuanzengWithPinkun(Integer pid);
}

