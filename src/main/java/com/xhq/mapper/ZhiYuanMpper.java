package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.ZhiYuan;

/*
* 志愿者Mapper接口
* */
public interface ZhiYuanMpper extends BaseMapper<ZhiYuan> {
}
