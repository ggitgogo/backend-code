package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.BigType;
import com.xhq.entity.ProductSwiperImage;


public interface ProductSwiperImageMapper extends BaseMapper<ProductSwiperImage> {
}
