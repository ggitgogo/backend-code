package com.xhq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhq.entity.Pinkun;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/*
* 贫困户Mapper接口
* */



@Mapper
public interface PinkunMapper extends BaseMapper<Pinkun>{
    @Select("SELECT * FROM pinkun WHERE pid = #{pid}")
    Pinkun findByPid(Integer pid);
}
