package com.xhq.config;

import com.xhq.intercepter.SysInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebAppConfigure implements WebMvcConfigurer {
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowCredentials(true)
				.allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE","OPTIONS")
				.maxAge(3600);
	}

	//图片与本地映射
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/image/swiper/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\swiperimgs\\");
		registry.addResourceHandler("/image/bigtype/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\bigtypeimgs\\");
		registry.addResourceHandler("/image/product/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\productimgs\\");
		registry.addResourceHandler("/image/productswiperimgs/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\productswiperimages\\");
		registry.addResourceHandler("/image/productIntroImgs/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\productIntroImgs\\");
		registry.addResourceHandler("/image/productParaImgs/**").addResourceLocations("file:D:\\ProgramData\\mallImage\\productParaImgs\\");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		String[] patterns=new String[]{"/adminLogin","/product/**","/bigtype/**","/user/**","/weixinpay/**"};
		registry.addInterceptor(sysInterceptor())
				.addPathPatterns("/**")
				.excludePathPatterns(patterns);
	}

	@Bean
	public SysInterceptor sysInterceptor(){
		return new SysInterceptor();
	}

}
