package com.xhq.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {
	//注入分页
	@Bean
	public PaginationInterceptor paginationIntercepter(){return new PaginationInterceptor();}
}
